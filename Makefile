CC = gcc
ICONS_DIR = data/icons
ICONS_DEST = icons/hicolor
ENTRY_DIR = data/applications
ENTRY_DEST = applications
LOCALE_DIR = po
UI_DIR = resources
UI_DEST = recbox-cubbyhole/ui
BUILD_DIR = build
PREFIX = /usr/share
EXEC = recbox-cubbyhole
NAME = recbox-cubbyhole
EXEC_DEST = /usr/bin
PACKAGE = `pkg-config --cflags gtk+-3.0`
LIBS = `pkg-config --libs gtk+-3.0`
SOURCE = src/main.c \
		 src/window.c \
		 src/dir_config.c \
		 src/dir_update.c \
         src/run_rsync.c
FLAGS = -rdynamic

program: $(SOURCE)
	@echo -e ":: Compiling app"
	install -d $(BUILD_DIR)/bin
	$(CC) $(PACKAGE) -o $(BUILD_DIR)/bin/$(NAME) $(SOURCE) $(LIBS) $(FLAGS)
	@echo -e ":: Compiling translation files"
	install -t $(BUILD_DIR)/locale -D $(LOCALE_DIR)/*
	install -d $(BUILD_DIR)/locale/en_GB/LC_MESSAGES
	install -d $(BUILD_DIR)/locale/pl/LC_MESSAGES
	msgfmt -c -v -o $(BUILD_DIR)/locale/en_GB/LC_MESSAGES/$(NAME).mo $(BUILD_DIR)/locale/en_GB.po
	msgfmt -c -v -o $(BUILD_DIR)/locale/pl/LC_MESSAGES/$(NAME).mo $(BUILD_DIR)/locale/pl.po

install:
	@echo -e ":: Installing executable"
	install -t $(EXEC_DEST) -D $(BUILD_DIR)/bin/*
	@echo -e ":: Installing UI files"
	install -t $(PREFIX)/$(UI_DEST) -D $(UI_DIR)/*
	@echo -e ":: Installing translation files"
	install -t $(PREFIX)/locale/en_GB/LC_MESSAGES/ -D $(BUILD_DIR)/locale/en_GB/LC_MESSAGES/*
	install -t $(PREFIX)/locale/pl/LC_MESSAGES/ -D $(BUILD_DIR)/locale/pl/LC_MESSAGES/*
	@echo -e ":: Installing icons"
	install -t $(PREFIX)/$(ICONS_DEST)/16x16/apps/ -D $(ICONS_DIR)/16x16/apps/*
	install -t $(PREFIX)/$(ICONS_DEST)/22x22/apps/ -D $(ICONS_DIR)/22x22/apps/*
	install -t $(PREFIX)/$(ICONS_DEST)/24x24/apps/ -D $(ICONS_DIR)/24x24/apps/*
	install -t $(PREFIX)/$(ICONS_DEST)/32x32/apps/ -D $(ICONS_DIR)/32x32/apps/*
	install -t $(PREFIX)/$(ICONS_DEST)/48x48/apps/ -D $(ICONS_DIR)/48x48/apps/*
	install -t $(PREFIX)/$(ICONS_DEST)/64x64/apps/ -D $(ICONS_DIR)/64x64/apps/*
	install -t $(PREFIX)/$(ICONS_DEST)/scalable/apps/ -D $(ICONS_DIR)/scalable/apps/*
	@echo -e ":: Updating icons cache"
	gtk-update-icon-cache -f -t /usr/share/icons/hicolor/
	@echo -e ":: Installing desktop entry"
	install -t $(PREFIX)/$(ENTRY_DEST)/ -D $(ENTRY_DIR)/*

clean:
	@echo ":: Removing Cubbyhole"
	rm $(EXEC_DEST)/$(NAME)
	rm -r $(PREFIX)/$(NAME)
	rm $(PREFIX)/locale/en_GB/LC_MESSAGES/$(NAME).mo
	rm $(PREFIX)/locale/pl/LC_MESSAGES/$(NAME).mo
	rm $(PREFIX)/$(ICONS_DEST)/16x16/apps/$(NAME).png
	rm $(PREFIX)/$(ICONS_DEST)/22x22/apps/$(NAME).png
	rm $(PREFIX)/$(ICONS_DEST)/24x24/apps/$(NAME).png
	rm $(PREFIX)/$(ICONS_DEST)/32x32/apps/$(NAME).png
	rm $(PREFIX)/$(ICONS_DEST)/48x48/apps/$(NAME).png
	rm $(PREFIX)/$(ICONS_DEST)/64x64/apps/$(NAME).png
	rm $(PREFIX)/$(ICONS_DEST)/scalable/apps/$(NAME).svg
	rm $(PREFIX)/$(ENTRY_DEST)/$(NAME).desktop
