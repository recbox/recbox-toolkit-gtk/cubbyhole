# Cubbyhole

#### Simple tool to back up recordings and DAW configs with rsync.

![cubbyhole](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/cubbyhole.png)

## Dependencies

- rsync
- GTK 3.24
- glib2 >= 2.74.0

## Installation

```
git clone https://gitlab.com/recbox/recbox-toolkit-gtk/cubbyhole.git
cd cubbyhole/
make
sudo make install
```

## Remove
```
sudo make clean
```
