#include <gtk/gtk.h>
#include <glib.h>

#include <sys/types.h>
#include <sys/stat.h>

const gchar *PathsGroupTitle = "Paths";
const gchar *WarningsGroupTitle = "Warnings";
const gchar *ErrorsGroupTitle = "Errors";

const gchar *ConfDir = "recbox-cubbyhole";
const gchar *ConfFile = "config";

const gchar *RecDir = "RecordsDirectory";
const gchar *RecBack = "RecordsBackup";
const gchar *DawConf = "DawConfig";
const gchar *DawBack = "DawBackup";

const gchar *RecBackupStatus = "RecBackupStatus";
const gchar *RecRestoreStatus = "RecRestoreStatus";
const gchar *DawBackupStatus = "DawBackupStatus";
const gchar *DawRestoreStatus = "DawRestoreStatus";

const gchar *RecBackupStatusError = "RecBackupStatusError";
const gchar *RecRestoreStatusError = "RecRestoreStatusError";
const gchar *DawBackupStatusError = "DawBackupStatusError";
const gchar *DawRestoreStatusError = "DawRestoreStatusError";

gchar *RecDirPath = "none";
gchar *RecBackPath = "none";
gchar *DawConfPath = "none";
gchar *DawBackPath = "none";

gchar *LoadRecDir;
gchar *LoadRecBack;
gchar *LoadDawConf;
gchar *LoadDawBack;

gint
CreateConf ()
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_set_string(key_file, PathsGroupTitle,
                        RecDir, RecDirPath);
  g_key_file_set_string(key_file, PathsGroupTitle,
                        RecBack, RecBackPath);
  g_key_file_set_string(key_file, PathsGroupTitle,
                        DawConf, DawConfPath);
  g_key_file_set_string(key_file, PathsGroupTitle, DawBack,
                        DawBackPath);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         RecBackupStatus, FALSE);
  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         RecRestoreStatus, FALSE);
  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         DawBackupStatus, FALSE);
  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         DawRestoreStatus, FALSE);

  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         RecBackupStatusError, FALSE);
  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         RecRestoreStatusError, FALSE);
  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         DawBackupStatusError, FALSE);
  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         DawRestoreStatusError, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  if (!g_key_file_load_from_file(key_file, conf_file, flags, &error))
  {
    printf("==> %s\n", error->message);
    g_error ("%s", error->message);
    g_key_file_free (key_file);
    return 1;
  }

  g_key_file_free(key_file);
  g_free(error);

  g_free(conf_file);
  g_free(conf_path);

  return 0;
}

gint
LoadPaths ()
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  if (!g_key_file_get_string(key_file, PathsGroupTitle, RecDir, &error)) {
    g_print("==> Missing Records Directory key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default path\n");

    g_key_file_set_string(key_file, PathsGroupTitle,
                          RecDir, RecDirPath);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string(key_file, PathsGroupTitle, RecBack, &error)) {
    g_print("==> Missing Records Backup key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default path\n");

    g_key_file_set_string(key_file, PathsGroupTitle,
                          RecBack, RecBackPath);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string(key_file, PathsGroupTitle, DawConf, &error)) {
    g_print("==> Missing DAW Config key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default path\n");

    g_key_file_set_string(key_file, PathsGroupTitle,
                          DawConf, DawConfPath);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string (key_file, PathsGroupTitle, DawBack, &error)) {
    g_print("==> Missing DAW Backup key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default path\n");

    g_key_file_set_string(key_file, PathsGroupTitle,
                          DawBack, DawBackPath);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string(key_file, WarningsGroupTitle, RecBackupStatus, &error)) {
    g_print("==> Missing Records Backup Status key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, WarningsGroupTitle,
                           RecBackupStatus, FALSE);

    if (!g_key_file_get_string(key_file, ErrorsGroupTitle, RecBackupStatusError, &error)) {
      g_print("==> Missing Records Backup Status Error key\n");

      g_clear_error (&error);

      g_print(" --> Setting up default boolean\n");

      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             RecBackupStatusError, TRUE);
    } else {
      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             RecBackupStatusError, TRUE);
    }

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string(key_file, WarningsGroupTitle, RecRestoreStatus, &error)) {
    g_print("==> Missing Records Restore Status key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, WarningsGroupTitle,
                           RecRestoreStatus, FALSE);

    if (!g_key_file_get_string(key_file, ErrorsGroupTitle, RecRestoreStatusError, &error)) {
      g_print("==> Missing Records Restore Status Error key\n");

      g_clear_error (&error);

      g_print(" --> Setting up default boolean\n");

      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             RecRestoreStatusError, TRUE);
    } else {
      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             RecRestoreStatusError, TRUE);
    }

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string (key_file, WarningsGroupTitle, DawBackupStatus, &error)) {
    g_print("==> Missing DAW Backup Status key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, WarningsGroupTitle,
                           DawBackupStatus, FALSE);

    if (!g_key_file_get_string (key_file, ErrorsGroupTitle, DawBackupStatusError, &error)) {
      g_print("==> Missing DAW Backup Status Error key\n");

      g_clear_error (&error);

      g_print(" --> Setting up default boolean\n");

      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             DawBackupStatusError, TRUE);
    } else {
      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             DawBackupStatusError, TRUE);
    }

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string (key_file, WarningsGroupTitle, DawRestoreStatus, &error)) {
    g_print("==> Missing DAW Restore Status key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, WarningsGroupTitle,
                           DawRestoreStatus, FALSE);

    if (!g_key_file_get_string (key_file, ErrorsGroupTitle, DawRestoreStatusError, &error)) {
      g_print("==> Missing DAW Restore Status Error key\n");

      g_clear_error (&error);

      g_print(" --> Setting up default boolean\n");

      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             DawRestoreStatusError, TRUE);
    } else {
      g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                             DawRestoreStatusError, TRUE);
    }

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string(key_file, ErrorsGroupTitle, RecBackupStatusError, &error)) {
    g_print("==> Missing Records Backup Status Error key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                           RecBackupStatusError, FALSE);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string(key_file, ErrorsGroupTitle, RecRestoreStatusError, &error)) {
    g_print("==> Missing Records Restore Status Error key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                           RecRestoreStatusError, FALSE);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string (key_file, ErrorsGroupTitle, DawBackupStatusError, &error)) {
    g_print("==> Missing DAW Backup Status Error key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                           DawBackupStatusError, FALSE);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  if (!g_key_file_get_string (key_file, ErrorsGroupTitle, DawRestoreStatusError, &error)) {
    g_print("==> Missing DAW Restore Status Error key\n");

    g_clear_error (&error);

    g_print(" --> Setting up default boolean\n");

    g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                           DawRestoreStatusError, FALSE);

    g_key_file_save_to_file(key_file, conf_file, &error);
  }

  LoadRecDir = g_key_file_get_string(key_file, PathsGroupTitle,
                                     RecDir, &error);
  LoadRecBack = g_key_file_get_string(key_file, PathsGroupTitle,
                                      RecBack, &error);
  LoadDawConf = g_key_file_get_string(key_file, PathsGroupTitle,
                                      DawConf, &error);
  LoadDawBack = g_key_file_get_string(key_file, PathsGroupTitle,
                                      DawBack, &error);

  g_key_file_free(key_file);
  g_free(error);

  g_free(conf_file);
  g_free(conf_path);
}

void
ConfCheck(gpointer data)
{
  g_print(":: Looking for config file\n");

  gchar *file_name;
  gchar *file_path;

  file_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  file_name = g_strconcat(file_path,"/", ConfFile, NULL);

  if (access(file_name, F_OK | R_OK))
  {
    g_print(":: Error while opening config file\n");

    if (mkdir (file_path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1)
    {
      CreateConf();
    }

    CreateConf();

    g_print("==> Config file restored\n");
  }

  g_free(file_name);
  g_free(file_path);
}
