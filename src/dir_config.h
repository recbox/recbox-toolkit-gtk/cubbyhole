#ifndef __DIRCONFIG_H
#define __DIRCONFIG_H

#include <gtk/gtk.h>

#define CBH_DIR_CONFIG_TYPE (cbh_dir_config_get_type())
void ConfCheck(gpointer data);
gint CreateConf();
gint ConfTest();
gint LoadPaths();

extern gchar *PathsGroupTitle;
extern gchar *WarningsGroupTitle;
extern gchar *ErrorsGroupTitle;
extern gchar *ConfDir;
extern gchar *ConfFile;

extern gchar *RecDir;
extern gchar *RecBack;
extern gchar *DawConf;
extern gchar *DawBack;

extern gchar *LoadRecDir;
extern gchar *LoadRecBack;
extern gchar *LoadDawConf;
extern gchar *LoadDawBack;

extern gchar *RecBackupStatus;
extern gchar *RecRestoreStatus;
extern gchar *DawBackupStatus;
extern gchar *DawRestoreStatus;

extern gchar *RecBackupStatusError;
extern gchar *RecRestoreStatusError;
extern gchar *DawBackupStatusError;
extern gchar *DawRestoreStatusError;
#endif
