#include <gtk/gtk.h>
#include "dir_config.h"

void
NewRecDir (GtkFileChooser *chooser,
           GtkLabel       *label,
           gpointer       data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;
  gchar *NewRecDirPath;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  NewRecDirPath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));

  g_key_file_set_string(key_file, PathsGroupTitle,
                        RecDir, NewRecDirPath);

  gtk_label_set_text(GTK_LABEL(label), NewRecDirPath);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);

  g_free(NewRecDirPath);
}

void
NewRecBack (GtkFileChooser *chooser,
            GtkLabel       *label,
            gpointer       data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;
  gchar *NewRecBackPath;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  NewRecBackPath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));

  g_key_file_set_string(key_file, PathsGroupTitle,
                        RecBack, NewRecBackPath);

  gtk_label_set_text(GTK_LABEL(label), NewRecBackPath);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);

  g_free(NewRecBackPath);
}


void
NewDawConf (GtkFileChooser *chooser,
            GtkLabel       *label,
            gpointer       data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;
  gchar *NewDawConfPath;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  NewDawConfPath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));

  g_key_file_set_string(key_file, PathsGroupTitle,
                        DawConf, NewDawConfPath);

  gtk_label_set_text(GTK_LABEL(label), NewDawConfPath);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);

  g_free(NewDawConfPath);
}


void
NewDawBack (GtkFileChooser *chooser,
            GtkLabel       *label,
            gpointer       data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;
  gchar *NewDawBackPath;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  NewDawBackPath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));

  g_key_file_set_string(key_file, PathsGroupTitle,
                        DawBack, NewDawBackPath);

  gtk_label_set_text(GTK_LABEL(label), NewDawBackPath);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);

  g_free(NewDawBackPath);
}

void
RecBackupTrue (GtkButton *rec_dir_cancel,
               gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         RecBackupStatus, TRUE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
RecBackupFalse (GtkButton *rec_backup_resume,
                gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         RecBackupStatus, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
RecRestoreTrue (GtkButton *rec_back_cancel,
                gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         RecRestoreStatus, TRUE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
RecRestoreFalse (GtkButton *rec_restore_resume,
                 gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         RecRestoreStatus, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
DawBackupTrue (GtkButton *daw_conf_cancel,
               gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         DawBackupStatus, TRUE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
DawBackupFalse (GtkButton *daw_backup_resume,
                gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         DawBackupStatus, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
DawRestoreTrue (GtkButton *daw_back_cancel,
                gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         DawRestoreStatus, TRUE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
DawRestoreFalse (GtkButton *daw_restore_resume,
                 gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, WarningsGroupTitle,
                         DawRestoreStatus, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
RecBackupErrorTrue (GtkButton *rec_backup_error,
                    gpointer data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         RecBackupStatusError, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
RecRestoreErrorTrue (GtkButton *rec_restore_error,
                     gpointer data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         RecRestoreStatusError, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
DawBackupErrorTrue (GtkButton *daw_backup_error,
                    gpointer data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         DawBackupStatusError, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}

void
DawRestoreErrorTrue (GtkButton *daw_restore_error,
                     gpointer  data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  g_key_file_set_boolean(key_file, ErrorsGroupTitle,
                         DawRestoreStatusError, FALSE);

  g_key_file_save_to_file(key_file, conf_file, &error);

  g_key_file_free(key_file);

  g_free(error);
  g_free(conf_file);
  g_free(conf_path);
}
