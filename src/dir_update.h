#ifndef __DIRUPDATE_H
#define __DIRUPDATE_H

#include <gtk/gtk.h>

#define CBH_DIR_UPDATE_TYPE (cbh_dir_update_get_type())
void NewRecDir       (GtkFileChooser *chooser,
                      gpointer data);

void NewRecBack      (GtkFileChooser *chooser,
                      gpointer data);

void NewDawConf      (GtkFileChooser *chooser,
                      gpointer data);

void NewDawBack      (GtkFileChooser *chooser,
                      gpointer data);

void RecBackupTrue   (GtkButton *rec_dir_cancel,
                      gpointer  data);

void RecBackupFalse  (GtkButton *rec_backup_resume,
                      gpointer  data);

void RecRestoreTrue  (GtkButton *rec_back_cancel,
                      gpointer  data);

void RecRestoreFalse (GtkButton *rec_restore_resume,
                      gpointer  data);

void DawBackupTrue   (GtkButton *daw_conf_cancel,
                      gpointer  data);

void DawBackupFalse  (GtkButton *daw_backup_resume,
                      gpointer  data);

void DawRestoreTrue  (GtkButton *daw_back_cancel,
                      gpointer  data);

void DawRestoreFalse (GtkButton *daw_restore_resume,
                      gpointer  data);
void
RecBackupErrorTrue (GtkButton *rec_backup_error,
                    gpointer data);
void
RecRestoreErrorTrue (GtkButton *rec_restore_error,
                     gpointer data);
void
DawBackupErrorTrue (GtkButton *daw_backup_error,
                    gpointer data);
void
DawRestoreErrorTrue (GtkButton *daw_restore_error,
                     gpointer data);
#endif
