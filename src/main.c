#include <gtk/gtk.h>

#include "window.h"
#include "run_rsync.h"
#include "dir_config.h"

static void
RsyncTest (gpointer data)
{
  GKeyFile *key_file;
  GKeyFileFlags flags;
  GError *error;
  gsize length;

  key_file = g_key_file_new();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  error = NULL;

  gchar *conf_file;
  gchar *conf_path;

  conf_path = g_strconcat(g_get_user_config_dir(),"/", ConfDir, NULL);
  conf_file = g_strconcat(conf_path,"/", ConfFile, NULL);

  g_key_file_load_from_file(key_file, conf_file,
                            flags, &error);

  if (g_key_file_get_boolean(key_file, WarningsGroupTitle, RecBackupStatus, &error) == TRUE) {
    g_print("==> Records backup canceled\n");
    gtk_widget_show(GTK_WIDGET(rec_dir_warning_label));
    gtk_widget_hide(GTK_WIDGET(rec_top_button_grid));
    gtk_widget_set_sensitive(GTK_WIDGET(rec_top_button_grid), FALSE);
  }

  if (g_key_file_get_boolean(key_file, WarningsGroupTitle, RecRestoreStatus, &error) == TRUE) {
    g_print ("==> Backup restoring canceled\n");
    gtk_widget_show(GTK_WIDGET(rec_back_warning_label));
    gtk_widget_hide(GTK_WIDGET(rec_top_button_grid));
    gtk_widget_set_sensitive(GTK_WIDGET(rec_top_button_grid), FALSE);
  }

  if (g_key_file_get_boolean(key_file, WarningsGroupTitle, DawBackupStatus, &error) == TRUE) {
    g_print ("==> Configs backup canceled\n");
    gtk_widget_show(GTK_WIDGET(daw_conf_warning_label));
    gtk_widget_hide(GTK_WIDGET(daw_top_button_grid));
    gtk_widget_set_sensitive(GTK_WIDGET(daw_top_button_grid), FALSE);
  }

  if (g_key_file_get_boolean(key_file, WarningsGroupTitle, DawRestoreStatus, &error) == TRUE) {
    g_print ("==> Configs restoring canceled\n");
    gtk_widget_show(GTK_WIDGET(daw_back_warning_label));
    gtk_widget_hide(GTK_WIDGET(daw_top_button_grid));
    gtk_widget_set_sensitive(GTK_WIDGET(daw_top_button_grid), FALSE);
  }

  if (g_key_file_get_boolean(key_file, ErrorsGroupTitle, RecBackupStatusError, &error) == TRUE) {
    gtk_widget_hide(GTK_WIDGET(rec_top_button_grid));
    gtk_widget_show(GTK_WIDGET(rec_dir_error_label));
  }

  if (g_key_file_get_boolean(key_file, ErrorsGroupTitle, RecRestoreStatusError, &error) == TRUE) {
    gtk_widget_hide(GTK_WIDGET(rec_top_button_grid));
    gtk_widget_show(GTK_WIDGET(rec_back_error_label));
  }

  if (g_key_file_get_boolean(key_file, ErrorsGroupTitle, DawBackupStatusError, &error) == TRUE) {
    gtk_widget_hide(GTK_WIDGET(daw_top_button_grid));
    gtk_widget_show(GTK_WIDGET(daw_conf_error_label));
  }

  if (g_key_file_get_boolean(key_file, ErrorsGroupTitle, DawRestoreStatusError, &error) == TRUE) {
    gtk_widget_hide(GTK_WIDGET(daw_top_button_grid));
    gtk_widget_show(GTK_WIDGET(daw_back_error_label));
  }

  g_key_file_free(key_file);
  g_free(error);

  g_free(conf_file);
  g_free(conf_path);
}

static void
LoadCSS (gpointer data)
{
  const gchar *StyleCSS = "/usr/share/recbox-cubbyhole/ui/style.css";

  GError *error = 0;

  GtkCssProvider *provider = gtk_css_provider_new();

  GdkDisplay *display = gdk_display_get_default();

  GdkScreen *screen = gdk_display_get_default_screen(display);

  gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER (provider),
                                            GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  gtk_css_provider_load_from_file(provider, g_file_new_for_path (StyleCSS),
                                  &error);

  g_object_unref(provider);

  g_free(error);
}

int
main (int   argc,
      char *argv[])
{
#ifdef GTK_SRCDIR
  g_chdir(GTK_SRCDIR);
#endif

  GtkApplication *app = gtk_application_new("com.gitlab.projektroot.recbox-cubbyhole",
                                            G_APPLICATION_DEFAULT_FLAGS);

  g_signal_connect(app, "activate", G_CALLBACK (ConfCheck), NULL);
  g_signal_connect(app, "activate", G_CALLBACK (LoadPaths), NULL);
  g_signal_connect(app, "activate", G_CALLBACK (LoadCSS), NULL);
  g_signal_connect(app, "activate", G_CALLBACK (OpenWindow), NULL);
  g_signal_connect(app, "activate", G_CALLBACK (LoadPaths), NULL);
  g_signal_connect (app, "activate", G_CALLBACK (RsyncTest), NULL);

  gint status = g_application_run(G_APPLICATION(app), argc, argv);

  g_object_unref(app);

  return status;
}
