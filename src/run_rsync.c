#include <gtk/gtk.h>

#include <dirent.h>
#include <stdbool.h>

#include "dir_config.h"
#include "run_rsync.h"
#include "window.h"

gint lock_window_backup;
gint lock_window_daw;

gpointer
RecDirRsyncDone ()
{
  gtk_spinner_stop(GTK_SPINNER(rec_dir_spinner));
  gtk_widget_hide(GTK_WIDGET(rec_dir_spinner));
  gtk_widget_hide(GTK_WIDGET(rec_dir_inprog));
  gtk_widget_hide(GTK_WIDGET(rec_dir_cancel));
  gtk_widget_show(GTK_WIDGET(rec_dir_done));
  gtk_widget_show(GTK_WIDGET(rec_dir_done_img));
  gtk_widget_show(GTK_WIDGET(rec_dir_close));
}

gpointer
RecDirIsEmpty ()
{
  gtk_widget_hide(GTK_WIDGET(rec_dir_spinner));
  gtk_widget_show(GTK_WIDGET(rec_dir_error_img));
  gtk_widget_show(GTK_WIDGET(rec_dir_empty));
  gtk_widget_show(GTK_WIDGET(rec_dir_close));

  g_print(":: Directory with Records is empty\n");
}

gpointer
RecDirAnError1 ()
{
  gtk_widget_hide(GTK_WIDGET(rec_dir_spinner));
  gtk_widget_show(GTK_WIDGET(rec_dir_error_img));
  gtk_widget_show(GTK_WIDGET(rec_dir_error_1));
  gtk_widget_show(GTK_WIDGET(rec_dir_close));

  g_print(":: Directory with Records doesn't exist\n");
}

gpointer
RecDirAnError2 ()
{
  gtk_widget_hide(GTK_WIDGET(rec_dir_spinner));
  gtk_widget_show(GTK_WIDGET(rec_dir_error_img));
  gtk_widget_show(GTK_WIDGET(rec_dir_error_2));
  gtk_widget_show(GTK_WIDGET(rec_dir_close));

  g_print(":: Directory with Records Backup doesn't exist\n");
}

gpointer
RecBackRsyncDone ()
{
  gtk_spinner_stop(GTK_SPINNER(rec_back_spinner));
  gtk_widget_hide(GTK_WIDGET(rec_back_spinner));
  gtk_widget_hide(GTK_WIDGET(rec_back_inprog));
  gtk_widget_hide(GTK_WIDGET(rec_back_cancel));
  gtk_widget_show(GTK_WIDGET(rec_back_done));
  gtk_widget_show(GTK_WIDGET(rec_back_done_img));
  gtk_widget_show(GTK_WIDGET(rec_back_close));
}

gpointer
RecBackIsEmpty ()
{
  gtk_widget_hide(GTK_WIDGET(rec_back_spinner));
  gtk_widget_show(GTK_WIDGET(rec_back_error_img));
  gtk_widget_show(GTK_WIDGET(rec_back_empty));
  gtk_widget_show(GTK_WIDGET(rec_back_close));

  g_print(":: Directory with Records Backup is empty\n");
}

gpointer
RecBackAnError1 ()
{
  gtk_widget_hide(GTK_WIDGET(rec_back_spinner));
  gtk_widget_show(GTK_WIDGET(rec_back_error_img));
  gtk_widget_show(GTK_WIDGET(rec_back_error_1));
  gtk_widget_show(GTK_WIDGET(rec_back_close));

  g_print(":: Directory with Records Backup doesn't exist\n");
}

gpointer
RecBackAnError2 ()
{
  gtk_widget_hide(GTK_WIDGET(rec_back_spinner));
  gtk_widget_show(GTK_WIDGET(rec_back_error_img));
  gtk_widget_show(GTK_WIDGET(rec_back_error_2));
  gtk_widget_show(GTK_WIDGET(rec_back_close));

  g_print(":: Directory with Records doesn't exist\n");
}

gpointer
DawConfRsyncDone ()
{
  gtk_spinner_stop(GTK_SPINNER(daw_conf_spinner));
  gtk_widget_hide(GTK_WIDGET(daw_conf_spinner));
  gtk_widget_hide(GTK_WIDGET(daw_conf_inprog));
  gtk_widget_hide(GTK_WIDGET(daw_conf_cancel));
  gtk_widget_show(GTK_WIDGET(daw_conf_done));
  gtk_widget_show(GTK_WIDGET(daw_conf_done_img));
  gtk_widget_show(GTK_WIDGET(daw_conf_close));
}

gpointer
DawConfIsEmpty ()
{
  gtk_widget_hide(GTK_WIDGET(daw_conf_spinner));
  gtk_widget_show(GTK_WIDGET(daw_conf_error_img));
  gtk_widget_show(GTK_WIDGET(daw_conf_empty));
  gtk_widget_show(GTK_WIDGET(daw_conf_close));

  g_print(":: Directory with DAW config files is empty\n");
}

gpointer
DawConfAnError1 ()
{
  gtk_widget_hide(GTK_WIDGET(daw_conf_spinner));
  gtk_widget_show(GTK_WIDGET(daw_conf_error_img));
  gtk_widget_show(GTK_WIDGET(daw_conf_error_1));
  gtk_widget_show(GTK_WIDGET(daw_conf_close));

  g_print(":: Directory with DAW config files doesn't exist\n");
}

gpointer
DawConfAnError2 ()
{
  gtk_widget_hide(GTK_WIDGET(daw_conf_spinner));
  gtk_widget_show(GTK_WIDGET(daw_conf_error_img));
  gtk_widget_show(GTK_WIDGET(daw_conf_error_2));
  gtk_widget_show(GTK_WIDGET(daw_conf_close));

  g_print(":: Directory with DAW config files Backup doesn't exist\n");
}

gpointer
DawBackRsyncDone ()
{
  gtk_spinner_stop(GTK_SPINNER(daw_back_spinner));
  gtk_widget_hide(GTK_WIDGET(daw_back_spinner));
  gtk_widget_hide(GTK_WIDGET(daw_back_inprog));
  gtk_widget_hide(GTK_WIDGET(daw_back_cancel));
  gtk_widget_show(GTK_WIDGET(daw_back_done));
  gtk_widget_show(GTK_WIDGET(daw_back_done_img));
  gtk_widget_show(GTK_WIDGET(daw_back_close));
}

gpointer
DawBackIsEmpty ()
{
  gtk_widget_hide(GTK_WIDGET(daw_back_spinner));
  gtk_widget_show(GTK_WIDGET(daw_back_error_img));
  gtk_widget_show(GTK_WIDGET(daw_back_empty));
  gtk_widget_show(GTK_WIDGET(daw_back_close));

  g_print(":: Directory with DAW config files Backup is empty\n");
}

gpointer
DawBackAnError1 ()
{
  gtk_widget_hide(GTK_WIDGET(daw_back_spinner));
  gtk_widget_show(GTK_WIDGET(daw_back_error_img));
  gtk_widget_show(GTK_WIDGET(daw_back_error_1));
  gtk_widget_show(GTK_WIDGET(daw_back_close));

  g_print(":: Directory with DAW config files Backup doesn't exist\n");
}

gpointer
DawBackAnError2 ()
{
  gtk_widget_hide(GTK_WIDGET(daw_back_spinner));
  gtk_widget_show(GTK_WIDGET(daw_back_error_img));
  gtk_widget_show(GTK_WIDGET(daw_back_error_2));
  gtk_widget_show(GTK_WIDGET(daw_back_close));

  g_print(":: Directory with DAW config file doesn't exist\n");
}

gboolean
LockWindow (GtkWindow *window,
            GdkEvent  *event,
            gpointer  data)
{
  g_print(":: Window locked\n");
  g_print("==> Syncing data with rsync\n");

  return TRUE;
}

gint
KillRecBackup (gpointer data)
{
  gchar pkill_cmd[512];

  sprintf(pkill_cmd, "pkill -9 -f 'rsync -rvui --delete %s/ %s/'",
          LoadRecDir, LoadRecBack);

  system(pkill_cmd);

  g_print(":: Records backup canceled\n");
}

gint
KillRecBackupRun (gpointer data)
{
  GError *error = NULL;
  GThread *rec_backup_thread;

  rec_backup_thread = g_thread_try_new(NULL, (GThreadFunc)KillRecBackup,
                                       data, &error);

  if (g_thread_try_new == NULL) {
    g_print(":: Error: KillRecBackup Gthread can't be created\n");
  }

  g_thread_unref(rec_backup_thread);
  g_free(error);
}

gint
KillRecRestore (gpointer data)
{
  gchar pkill_cmd[512];

  sprintf(pkill_cmd, "pkill -9 -f 'rsync -rvui --delete %s/ %s/'",
          LoadRecBack, LoadRecDir);

  system(pkill_cmd);

  g_print(":: Records restoring canceled\n");
}

gint
KillRecRestoreRun (gpointer data)
{
  GError *error = NULL;
  GThread *rec_restore_thread;

  rec_restore_thread = g_thread_try_new(NULL, (GThreadFunc)KillRecRestore,
                                        data, &error);

  if (g_thread_try_new == NULL) {
    g_print(":: Error: KillRecRestoreRun Gthread can't be created\n");
  }

  g_thread_unref(rec_restore_thread);
  g_free(error);
}

gint
KillDawBackup (gpointer data)
{
  gchar pkill_cmd[512];

  sprintf(pkill_cmd, "pkill -9 -f 'rsync -rvui --delete %s/ %s/'",
          LoadDawConf, LoadDawBack);

  system(pkill_cmd);

  g_print(":: DAW backup canceled\n");
}

gint
KillDawBackupRun (gpointer data)
{
  GError *error = NULL;
  GThread *daw_backup_thread;

  daw_backup_thread = g_thread_try_new(NULL, (GThreadFunc)KillDawBackup,
                                       data, &error);

  if (g_thread_try_new == NULL) {
    g_print (":: Error: KillDawBackup Gthread can't be created\n");
  }

  g_thread_unref(daw_backup_thread);
  g_free(error);
}

gint
KillDawRestore (gpointer data)
{
  gchar pkill_cmd[512];

  sprintf(pkill_cmd, "pkill -9 -f 'rsync -rvui --delete %s/ %s/'",
          LoadDawBack, LoadDawConf);

  system(pkill_cmd);

  g_print(":: DAW restoring canceled\n");
}

gint
KillDawRestoreRun (gpointer data)
{
  GError *error = NULL;
  GThread *daw_restore_thread;

  daw_restore_thread = g_thread_try_new(NULL, (GThreadFunc)KillDawRestore,
                                        data, &error);

  if (g_thread_try_new == NULL) {
    g_print(":: Error: KillDawRestore Gthread can't be created\n");
  }

  g_thread_unref(daw_restore_thread);
  g_free(error);
}

gint
RecDirRsync (gpointer data)
{
  gchar rsync_cmd[512];

  g_print(":: Backup process start\n");

  lock_window_backup = g_signal_connect(GTK_WINDOW(window), "delete-event",
                                        G_CALLBACK (LockWindow), NULL);

  sprintf(rsync_cmd, "rsync -rvui --delete %s/ %s/",
          LoadRecDir, LoadRecBack);

  g_print("==> %s\n", rsync_cmd);

  system(rsync_cmd);

  g_signal_handler_disconnect(window, lock_window_backup);

  RecDirRsyncDone();
}

void
RecDirSpinnerStart (GtkSpinner *rec_dir_spinner,
                    gpointer   data)
{
  DIR* rec_dir = opendir(LoadRecDir);
  DIR* back_dir = opendir(LoadRecBack);

  if (rec_dir) {

    int rec_dir_count = 0;
    struct dirent *rec_dir_entry;

    while ((rec_dir_entry = readdir(rec_dir)) != NULL) {
      rec_dir_count++;
    }

    if (rec_dir_count >= 3) {
      if (back_dir) {

        GError *error = NULL;
        GThread *rec_dir_thread;

        gtk_widget_show(GTK_WIDGET(rec_dir_spinner));
        gtk_widget_show(GTK_WIDGET(rec_dir_inprog));
        gtk_widget_show(GTK_WIDGET(rec_dir_cancel));
        gtk_spinner_start(GTK_SPINNER(rec_dir_spinner));

        rec_dir_thread = g_thread_try_new(NULL, (GThreadFunc)RecDirRsync,
                                          data, &error);

        if (g_thread_try_new == NULL) {
          g_print(":: Error: RecDirRsync Gthread can't be created\n");
        }

        g_thread_unref(rec_dir_thread);
        g_free(error);

      } else if (ENOENT == errno) {
        RecDirAnError2();
      } else {
        g_print(":: Directory with Records Backup can't be opened for unspecified reason\n");
      }
    } else {
      RecDirIsEmpty();
    }

    g_free(rec_dir_entry);

  } else if (ENOENT == errno) {
    RecDirAnError1();
  } else {
    g_print(":: Directory with Records can't be opened for unspecified reason\n");
  }

  closedir(rec_dir);
  closedir(back_dir);
}

gint
RecBackRsync (gpointer data)
{
  gchar rsync_cmd[512];

  g_print(":: Syncing process start\n");

  lock_window_backup = g_signal_connect(GTK_WINDOW(window), "delete-event",
                                        G_CALLBACK(LockWindow), NULL);

  sprintf(rsync_cmd, "rsync -rvui --delete %s/ %s/", LoadRecBack, LoadRecDir);
  g_print("==> %s\n", rsync_cmd);

  system(rsync_cmd);

  g_signal_handler_disconnect(window, lock_window_backup);

  RecBackRsyncDone();
}

void
RecBackSpinnerStart (GtkSpinner *rec_back_spinner,
                     gpointer   data)
{
  DIR* rec_dir = opendir(LoadRecDir);
  DIR* back_dir = opendir(LoadRecBack);

  if (back_dir) {

    int back_dir_count = 0;
    struct dirent *back_dir_entry;

    while ((back_dir_entry = readdir(back_dir)) != NULL) {
      back_dir_count++;
    }

    if (back_dir_count >= 3) {
      if (rec_dir) {

        GError *error = NULL;
        GThread *rec_back_thread;

        gtk_widget_show(GTK_WIDGET(rec_back_spinner));
        gtk_widget_show(GTK_WIDGET(rec_back_inprog));
        gtk_widget_show(GTK_WIDGET(rec_back_cancel));
        gtk_spinner_start(GTK_SPINNER(rec_back_spinner));

        rec_back_thread = g_thread_try_new(NULL, (GThreadFunc)RecBackRsync,
                                           data, &error);

        if (g_thread_try_new == NULL) {
          g_print(":: Error: RecBackRsync Gthread can't be created\n");
        }

        g_thread_unref(rec_back_thread);
        g_free(error);

      } else if (ENOENT == errno) {
        RecBackAnError2();
      } else {
        g_print(":: Directory with Records can't be opened for unspecified reason\n");
      }
    } else {
      RecBackIsEmpty();
    }

    g_free(back_dir_entry);

  } else if (ENOENT == errno) {
    RecBackAnError1();
  } else {
    g_print(":: Directory with Records Backup can't be opened for unspecified reason\n");
  }

  closedir(rec_dir);
  closedir(back_dir);
}

gint
DawConfRsync (gpointer data)
{
  gchar rsync_cmd[512];

  g_print(":: Backup process start\n");

  lock_window_daw = g_signal_connect(GTK_WINDOW(window), "delete-event",
                                     G_CALLBACK(LockWindow), NULL);

  sprintf(rsync_cmd, "rsync -rvui --delete %s/ %s/",
          LoadDawConf, LoadDawBack);

  g_print("==> %s\n", rsync_cmd);

  system(rsync_cmd);

  g_signal_handler_disconnect(window, lock_window_daw);

  DawConfRsyncDone();
}

void
DawConfSpinnerStart (GtkSpinner *daw_conf_spinner,
                     gpointer   data)
{
  DIR* daw_conf = opendir(LoadDawConf);
  DIR* daw_back = opendir(LoadDawBack);

  if (daw_conf) {

    int daw_conf_count = 0;
    struct dirent *daw_conf_entry;

    while ((daw_conf_entry = readdir(daw_conf)) != NULL) {
      daw_conf_count++;
    }

    if (daw_conf_count >= 3) {
      if (daw_back) {

        GError *error = NULL;
        GThread *daw_conf_thread;

        gtk_widget_show(GTK_WIDGET(daw_conf_spinner));
        gtk_widget_show(GTK_WIDGET(daw_conf_inprog));
        gtk_widget_show(GTK_WIDGET(daw_conf_cancel));
        gtk_spinner_start(GTK_SPINNER(daw_conf_spinner));

        daw_conf_thread = g_thread_try_new(NULL, (GThreadFunc)DawConfRsync,
                                           data, &error);

        if (g_thread_try_new == NULL) {
          g_print(":: Error: DawConfRsync Gthread can't be created\n");
        }

        g_thread_unref(daw_conf_thread);
        g_free(error);

      } else if (ENOENT == errno) {
        DawConfAnError2();
      } else {
        g_print(":: DAW configs backup directory can't be opened for unspecified reason\n");
      }
    } else {
      DawConfIsEmpty();
    }

    g_free(daw_conf_entry);

  } else if (ENOENT == errno) {
    DawConfAnError1();
  } else {
    g_print(":: Directory with DAW configs can't be opened for unspecified reason\n");
  }

  closedir(daw_conf);
  closedir(daw_back);
}

gint
DawBackRsync (gpointer data)
{
  gchar rsync_cmd[512];

  g_print(":: Backup process start\n");

  lock_window_daw = g_signal_connect(GTK_WINDOW(window), "delete-event",
                                     G_CALLBACK(LockWindow), NULL);

  sprintf(rsync_cmd, "rsync -rvui --delete %s/ %s/",
          LoadDawBack, LoadDawConf);

  g_print("==> %s\n", rsync_cmd);

  system(rsync_cmd);

  g_signal_handler_disconnect(window, lock_window_daw);

  DawBackRsyncDone();
}

void
DawBackSpinnerStart (GtkSpinner *daw_back_spinner,
                     gpointer   data)
{
  DIR* daw_conf = opendir(LoadDawConf);
  DIR* daw_back = opendir(LoadDawBack);

  if (daw_back) {

    int daw_back_count = 0;
    struct dirent *daw_back_entry;

    while ((daw_back_entry = readdir(daw_back)) != NULL) {
      daw_back_count++;
    }

    if (daw_back_count >= 3) {
      if (daw_conf) {

        GError *error = NULL;
        GThread *daw_back_thread;

        gtk_widget_show(GTK_WIDGET(daw_back_spinner));
        gtk_widget_show(GTK_WIDGET(daw_back_inprog));
        gtk_widget_show(GTK_WIDGET(daw_back_cancel));
        gtk_spinner_start(GTK_SPINNER(daw_back_spinner));

        daw_back_thread = g_thread_try_new(NULL, (GThreadFunc)DawBackRsync,
                                           data, &error);

        if (g_thread_try_new == NULL) {
          g_print(":: Error: DawBackRsync Gthread can't be created\n");
        }

        g_thread_unref(daw_back_thread);
        g_free(error);

      } else if (ENOENT == errno) {
        DawBackAnError2();
      } else {
        g_print(":: Directory with DAW config files can't be opened for unspecified reason\n");
      }
    } else {
      DawBackIsEmpty();
    }

    g_free(daw_back_entry);

  } else if (ENOENT == errno) {
    DawBackAnError1();
  } else {
    g_print(":: Directory with DAW config files Backup can't be opened for unspecified reason\n");
  }

  closedir(daw_conf);
  closedir(daw_back);
}
