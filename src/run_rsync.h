#ifndef __RUNRSYNC_H
#define __RUNRSYNC_H

#include <gtk/gtk.h>

#define CBH_RUN_RSYNC_TYPE (cbh_run_rsync_get_type())
int RecDirRsync (gpointer data);
int RecBackRsync (gpointer data);
int DawConfRsync (gpointer data);
int DawBackRsync (gpointer data);

int KillRecBackup (gpointer data);
int KillRecRestore (gpointer data);
int KillDawBackup (gpointer data);
int KillDawRestore (gpointer data);

int KillRecBackupRun (gpointer data);
int KillRecRestoreRun (gpointer data);
int KillDawBackupRun (gpointer data);
int KillDawRestoreRun (gpointer data);

void StopSpinner (GtkSpinner *spinner, gpointer data);
void RecDirSpinnerStart (GtkSpinner *spinner, gpointer data);
void RecBackSpinnerStart (GtkSpinner *spinner, gpointer data);
void DawConfSpinnerStart (GtkSpinner *spinner, gpointer data);
void DawBackSpinnerStart (GtkSpinner *spinner, gpointer data);
#endif
