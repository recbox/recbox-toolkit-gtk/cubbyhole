#include <gtk/gtk.h>
#include <libintl.h>
#include <locale.h>

#include "dir_update.h"
#include "dir_config.h"
#include "run_rsync.h"

GObject *window;
GObject *button;

GObject *rec_top_button_grid;
GObject *daw_top_button_grid;

GObject *rec_dir_label;
GObject *rec_dir_warning_label;
GObject *rec_dir_error_label;
GObject *rec_dir_spinner_box;
GObject *rec_dir_spinner;
GObject *rec_dir_inprog;
GObject *rec_dir_done;
GObject *rec_dir_done_img;
GObject *rec_dir_error_1;
GObject *rec_dir_error_2;
GObject *rec_dir_error_img;
GObject *rec_dir_empty;
GObject *rec_dir_close;
GObject *rec_dir_cancel;

GObject *rec_back_label;
GObject *rec_back_warning_label;
GObject *rec_back_error_label;
GObject *rec_back_spinner_box;
GObject *rec_back_spinner;
GObject *rec_back_inprog;
GObject *rec_back_done;
GObject *rec_back_done_img;
GObject *rec_back_error_1;
GObject *rec_back_error_2;
GObject *rec_back_error_img;
GObject *rec_back_empty;
GObject *rec_back_close;
GObject *rec_back_cancel;

GObject *daw_conf_label;
GObject *daw_conf_warning_label;
GObject *daw_conf_error_label;
GObject *daw_conf_spinner_box;
GObject *daw_conf_spinner;
GObject *daw_conf_inprog;
GObject *daw_conf_done;
GObject *daw_conf_done_img;
GObject *daw_conf_error_1;
GObject *daw_conf_error_2;
GObject *daw_conf_error_img;
GObject *daw_conf_empty;
GObject *daw_conf_close;
GObject *daw_conf_cancel;

GObject *daw_back_label;
GObject *daw_back_warning_label;
GObject *daw_back_error_label;
GObject *daw_back_spinner_box;
GObject *daw_back_spinner;
GObject *daw_back_inprog;
GObject *daw_back_done;
GObject *daw_back_done_img;
GObject *daw_back_error_1;
GObject *daw_back_error_2;
GObject *daw_back_error_img;
GObject *daw_back_empty;
GObject *daw_back_close;
GObject *daw_back_cancel;

static void
LockRecTopGrid (GtkButton *rec_top_button_grid,
                gpointer  data)
{
  gtk_widget_set_sensitive (GTK_WIDGET(rec_top_button_grid), FALSE);
}

static void
LockDawTopGrid (GtkButton *rec_top_button_grid,
                gpointer  data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(rec_top_button_grid), FALSE);
}

static void
UnlockRecTopGrid (GtkButton *rec_top_button_grid,
                  gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(rec_top_button_grid), TRUE);
}

static void
UnlockDawTopGrid (GtkButton *daw_top_button_grid,
                  gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(daw_top_button_grid), TRUE);
}

static void
RecBackupButton (GtkButton *rec_dir_close,
                 gpointer  data)
{
  gtk_widget_hide(GTK_WIDGET(rec_dir_spinner_box));
  gtk_widget_hide(GTK_WIDGET(rec_dir_inprog));
  gtk_widget_hide(GTK_WIDGET(rec_dir_error_1));
  gtk_widget_hide(GTK_WIDGET(rec_dir_error_2));
  gtk_widget_hide(GTK_WIDGET(rec_dir_error_img));
  gtk_widget_hide(GTK_WIDGET(rec_dir_empty));
  gtk_widget_hide(GTK_WIDGET(rec_dir_close));
  gtk_widget_show(GTK_WIDGET(rec_dir_label));
}

static void
RecRestoreButton (GtkButton *rec_back_close,
                  gpointer  data)
{
  gtk_widget_hide(GTK_WIDGET(rec_back_spinner_box));
  gtk_widget_hide(GTK_WIDGET(rec_back_inprog));
  gtk_widget_hide(GTK_WIDGET(rec_back_error_1));
  gtk_widget_hide(GTK_WIDGET(rec_back_error_2));
  gtk_widget_hide(GTK_WIDGET(rec_back_error_img));
  gtk_widget_hide(GTK_WIDGET(rec_back_empty));
  gtk_widget_hide(GTK_WIDGET(rec_back_close));
  gtk_widget_show(GTK_WIDGET(rec_back_label));
}

static void
DawBackupButton (GtkButton *daw_conf_close,
                 gpointer  data)
{
  gtk_widget_hide(GTK_WIDGET(daw_conf_spinner_box));
  gtk_widget_hide(GTK_WIDGET(daw_conf_inprog));
  gtk_widget_hide(GTK_WIDGET(daw_conf_error_1));
  gtk_widget_hide(GTK_WIDGET(daw_conf_error_2));
  gtk_widget_hide(GTK_WIDGET(daw_conf_error_img));
  gtk_widget_hide(GTK_WIDGET(daw_conf_empty));
  gtk_widget_hide(GTK_WIDGET(daw_conf_close));
  gtk_widget_show(GTK_WIDGET(daw_conf_label));
}

static void
DawRestoreButton (GtkButton *daw_back_close,
                  gpointer  data)
{
  gtk_widget_hide (GTK_WIDGET (daw_back_spinner_box));
  gtk_widget_hide(GTK_WIDGET(daw_back_inprog));
  gtk_widget_hide(GTK_WIDGET(daw_back_error_1));
  gtk_widget_hide(GTK_WIDGET(daw_back_error_2));
  gtk_widget_hide(GTK_WIDGET(daw_back_error_img));
  gtk_widget_hide(GTK_WIDGET(daw_back_empty));
  gtk_widget_hide(GTK_WIDGET(daw_back_close));
  gtk_widget_show(GTK_WIDGET(daw_back_label));
}

static void
RecBackupCancel (GtkButton *rec_dir_cancel,
                 gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(rec_dir_label));
  gtk_widget_show(GTK_WIDGET(rec_dir_warning_label));
  gtk_widget_hide(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(rec_dir_spinner_box));
  gtk_widget_hide(GTK_WIDGET(rec_dir_inprog));
  gtk_widget_hide(GTK_WIDGET(rec_dir_close));
}

static void
RecRestoreCancel (GtkButton *rec_back_cancel,
                  gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(rec_back_label));
  gtk_widget_show(GTK_WIDGET(rec_back_warning_label));
  gtk_widget_hide(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(rec_back_spinner_box));
  gtk_widget_hide(GTK_WIDGET(rec_back_inprog));
  gtk_widget_hide(GTK_WIDGET(rec_back_close));
}

static void
DawBackupCancel (GtkButton *daw_conf_cancel,
                 gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(daw_conf_label));
  gtk_widget_show(GTK_WIDGET(daw_conf_warning_label));
  gtk_widget_hide(GTK_WIDGET(daw_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(daw_conf_spinner_box));
  gtk_widget_hide(GTK_WIDGET(daw_conf_inprog));
  gtk_widget_hide(GTK_WIDGET(daw_conf_close));
}

static void
DawRestoreCancel (GtkButton *daw_back_cancel,
                  gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(daw_back_label));
  gtk_widget_show(GTK_WIDGET(daw_back_warning_label));
  gtk_widget_hide(GTK_WIDGET(daw_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(daw_back_spinner_box));
  gtk_widget_hide(GTK_WIDGET(daw_back_inprog));
  gtk_widget_hide(GTK_WIDGET(daw_back_close));
}

static void
RecResumeBackup (GtkButton *rec_backup_resume,
                 gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(rec_dir_label));
  gtk_widget_hide(GTK_WIDGET(rec_dir_warning_label));
  gtk_widget_hide(GTK_WIDGET(rec_dir_done));
  gtk_widget_hide(GTK_WIDGET(rec_dir_done_img));
  gtk_widget_hide(GTK_WIDGET(rec_dir_close));
}

static void
RecResumeRestore (GtkButton *rec_restore_resume,
                  gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(rec_back_label));
  gtk_widget_hide(GTK_WIDGET(rec_back_warning_label));
  gtk_widget_hide(GTK_WIDGET(rec_back_done));
  gtk_widget_hide(GTK_WIDGET(rec_back_done_img));
  gtk_widget_hide(GTK_WIDGET(rec_back_close));
}

static void
DawResumeBackup (GtkButton *daw_backup_resume,
                 gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(daw_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(daw_conf_label));
  gtk_widget_hide(GTK_WIDGET(daw_conf_warning_label));
  gtk_widget_hide(GTK_WIDGET(daw_conf_done));
  gtk_widget_hide(GTK_WIDGET(daw_conf_done_img));
  gtk_widget_hide(GTK_WIDGET(daw_conf_close));
}

static void
DawResumeRestore (GtkButton *daw_restore_resume,
                  gpointer  data)
{
  gtk_widget_show(GTK_WIDGET(daw_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(daw_back_label));
  gtk_widget_hide(GTK_WIDGET(daw_back_warning_label));
  gtk_widget_hide(GTK_WIDGET(daw_back_done));
  gtk_widget_hide(GTK_WIDGET(daw_back_done_img));
  gtk_widget_hide(GTK_WIDGET(daw_back_close));
}

static void
RecBackupErrorCloseButton (GtkButton *rec_backup_error,
                           gpointer data)
{
  gtk_widget_show(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(rec_dir_error_label));
}

static void
RecRestoreErrorCloseButton (GtkButton *rec_restore_error,
                            gpointer data)
{
  gtk_widget_show(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(rec_back_error_label));
}

static void
DawBackupErrorCloseButton (GtkButton *daw_backup_error,
                           gpointer data)
{
  gtk_widget_show(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(daw_conf_error_label));
}

static void
DawRestoreErrorCloseButton (GtkButton *daw_restore_error,
                           gpointer data)
{
  gtk_widget_show(GTK_WIDGET(rec_top_button_grid));
  gtk_widget_hide(GTK_WIDGET(daw_back_error_label));
}

void
OpenWindow (GtkApplication *app,
            gpointer       data)
{
  const gchar *GladeFile = "/usr/share/recbox-cubbyhole/ui/window.glade";

  const gchar *domain = "recbox-cubbyhole";
  const gchar *locale_path = "/usr/share/locale";

  GtkBuilder *builder;
  GObject *label;
  GObject *button_backup;
  GObject *button_restore;

  GObject *rec_backup_resume;
  GObject *rec_restore_resume;
  GObject *daw_backup_resume;
  GObject *daw_restore_resume;

  GObject *rec_backup_error;
  GObject *rec_restore_error;
  GObject *daw_backup_error;
  GObject *daw_restore_error;

  builder = gtk_builder_new();

  bindtextdomain(domain, locale_path);
  textdomain(domain);
  setlocale(LC_ALL, "");

  gtk_builder_set_translation_domain(builder, domain);
  gtk_builder_add_from_file(builder, GladeFile, NULL);

  window = gtk_builder_get_object(builder, "MainWindow");
  gtk_window_set_application(GTK_WINDOW(window), app);

  rec_top_button_grid = gtk_builder_get_object(builder, "RecTopButtonsGrid");
  daw_top_button_grid = gtk_builder_get_object(builder, "DawTopButtonsGrid");

  // Records - Backup Button
  button_backup = gtk_builder_get_object(builder, "RecBackButton");
  button_restore = gtk_builder_get_object(builder, "RecRestoreButton");
  rec_dir_close = gtk_builder_get_object(builder, "RecDirSpinnerBoxClose");
  rec_dir_cancel = gtk_builder_get_object(builder, "RecDirSpinnerBoxCancel");

  rec_backup_resume = gtk_builder_get_object(builder, "RecBackupResume");
  rec_backup_error = gtk_builder_get_object(builder, "RecBackupErrorClose");

  rec_dir_spinner_box = gtk_builder_get_object(builder, "RecDirSpinnerBox");
  rec_dir_spinner = gtk_builder_get_object(builder, "RecDirSpinner");

  rec_dir_label = gtk_builder_get_object(builder, "RecDirShowPath");
  rec_dir_warning_label = gtk_builder_get_object(builder, "RecBackupWarning");
  rec_dir_error_label = gtk_builder_get_object(builder, "RecBackupError");
  gtk_label_set_text(GTK_LABEL(rec_dir_label), LoadRecDir);

  rec_dir_inprog = gtk_builder_get_object(builder, "RecDirWorkInProgress");
  rec_dir_done = gtk_builder_get_object(builder, "RecDirDone");
  rec_dir_done_img = gtk_builder_get_object(builder, "RecDirDoneImg");
  rec_dir_error_1 = gtk_builder_get_object(builder, "RecDirError1");
  rec_dir_error_2 = gtk_builder_get_object(builder, "RecDirError2");
  rec_dir_error_img = gtk_builder_get_object(builder, "RecDirErrorImg");
  rec_dir_empty = gtk_builder_get_object(builder, "RecDirEmpty");

  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(LockRecTopGrid),rec_top_button_grid);

  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_hide), rec_dir_label);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_hide), rec_dir_done);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_hide), rec_dir_done_img);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_show), rec_dir_spinner_box);

  g_signal_connect(button_backup, "clicked",
                   G_CALLBACK(LoadPaths), NULL);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(RecDirSpinnerStart), rec_dir_spinner);

  g_signal_connect_swapped(rec_dir_close, "clicked",
                           G_CALLBACK(UnlockRecTopGrid), rec_top_button_grid);
  g_signal_connect_swapped(rec_dir_close, "clicked",
                           G_CALLBACK(RecBackupButton), rec_dir_close);

  g_signal_connect_swapped(rec_dir_cancel, "clicked",
                           G_CALLBACK(KillRecBackupRun), NULL);
  g_signal_connect_swapped(rec_dir_cancel, "clicked",
                           G_CALLBACK(RecBackupCancel), rec_dir_cancel);
  g_signal_connect(rec_dir_cancel, "clicked",
                   G_CALLBACK(RecBackupTrue), NULL);

  g_signal_connect(rec_backup_resume, "clicked",
                   G_CALLBACK(RecBackupFalse), NULL);
  g_signal_connect_swapped(rec_backup_resume, "clicked",
                           G_CALLBACK(RecResumeBackup), rec_backup_resume);
  g_signal_connect_swapped(rec_backup_resume, "clicked",
                           G_CALLBACK(gtk_widget_show), rec_dir_spinner_box);
  g_signal_connect_swapped(rec_backup_resume, "clicked",
                           G_CALLBACK(RecDirSpinnerStart), rec_dir_spinner);

  g_signal_connect(rec_backup_error, "clicked",
                   G_CALLBACK(RecBackupErrorTrue), NULL);
  g_signal_connect_swapped(rec_backup_error, "clicked",
                           G_CALLBACK(RecBackupErrorCloseButton), rec_backup_error);

  // Records - Restore Button
  button_backup = gtk_builder_get_object(builder, "RecBackButton");
  button_restore = gtk_builder_get_object(builder, "RecRestoreButton");
  rec_back_close = gtk_builder_get_object(builder, "RecBackSpinnerBoxClose");
  rec_back_cancel = gtk_builder_get_object(builder, "RecBackSpinnerBoxCancel");

  rec_restore_resume = gtk_builder_get_object(builder, "RecRestoreResume");
  rec_restore_error = gtk_builder_get_object(builder, "RecRestoreErrorClose");

  rec_back_spinner_box = gtk_builder_get_object(builder, "RecBackSpinnerBox");
  rec_back_spinner = gtk_builder_get_object(builder, "RecBackSpinner");

  rec_back_label = gtk_builder_get_object(builder, "RecBackShowPath");
  rec_back_warning_label = gtk_builder_get_object(builder, "RecRestoreWarning");
  rec_back_error_label = gtk_builder_get_object(builder, "RecRestoreError");
  gtk_label_set_text(GTK_LABEL(rec_back_label), LoadRecBack);

  rec_back_inprog = gtk_builder_get_object(builder, "RecBackWorkInProgress");
  rec_back_done = gtk_builder_get_object(builder, "RecBackDone");
  rec_back_done_img = gtk_builder_get_object(builder, "RecBackDoneImg");
  rec_back_error_1 = gtk_builder_get_object(builder, "RecBackError1");
  rec_back_error_2 = gtk_builder_get_object(builder, "RecBackError2");
  rec_back_error_img = gtk_builder_get_object(builder, "RecBackErrorImg");
  rec_back_empty = gtk_builder_get_object(builder, "RecBackEmpty");

  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(LockRecTopGrid), rec_top_button_grid);

  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_hide), rec_back_label);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_hide), rec_back_done);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_hide), rec_back_done_img);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_show), rec_back_spinner_box);

  g_signal_connect(button_restore, "clicked",
                   G_CALLBACK(LoadPaths), NULL);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(RecBackSpinnerStart), rec_back_spinner);

  g_signal_connect_swapped(rec_back_close, "clicked",
                           G_CALLBACK(UnlockRecTopGrid), rec_top_button_grid);
  g_signal_connect_swapped(rec_back_close, "clicked",
                           G_CALLBACK(RecRestoreButton), rec_back_close);

  g_signal_connect_swapped(rec_back_cancel, "clicked",
                           G_CALLBACK(KillRecRestoreRun), NULL);
  g_signal_connect_swapped(rec_back_cancel, "clicked",
                           G_CALLBACK(RecRestoreCancel), rec_back_cancel);
  g_signal_connect(rec_back_cancel, "clicked",
                   G_CALLBACK(RecRestoreTrue), NULL);

  g_signal_connect(rec_restore_resume, "clicked",
                   G_CALLBACK(RecRestoreFalse), NULL);
  g_signal_connect_swapped(rec_restore_resume, "clicked",
                           G_CALLBACK(RecResumeRestore), rec_restore_resume);
  g_signal_connect_swapped(rec_restore_resume, "clicked",
                           G_CALLBACK(gtk_widget_show), rec_back_spinner_box);
  g_signal_connect_swapped(rec_restore_resume, "clicked",
                           G_CALLBACK(RecBackSpinnerStart), rec_back_spinner);

  g_signal_connect(rec_restore_error, "clicked",
                   G_CALLBACK(RecRestoreErrorTrue), NULL);
  g_signal_connect_swapped(rec_restore_error, "clicked",
                           G_CALLBACK(RecRestoreErrorCloseButton), rec_restore_error);

  // DAW - Backup Button
  button_backup = gtk_builder_get_object(builder, "DawBackButton");
  button_restore = gtk_builder_get_object(builder, "DawRestoreButton");
  daw_conf_close = gtk_builder_get_object(builder, "DawConfSpinnerBoxClose");
  daw_conf_cancel = gtk_builder_get_object(builder, "DawConfSpinnerBoxCancel");

  daw_backup_resume = gtk_builder_get_object(builder, "DawBackupResume");
  daw_backup_error = gtk_builder_get_object(builder, "DawBackupErrorClose");

  daw_conf_spinner_box = gtk_builder_get_object(builder, "DawConfSpinnerBox");
  daw_conf_spinner = gtk_builder_get_object(builder, "DawConfSpinner");

  daw_conf_label = gtk_builder_get_object(builder, "DawConfShowPath");
  daw_conf_warning_label = gtk_builder_get_object(builder, "DawBackupWarning");
  daw_conf_error_label = gtk_builder_get_object(builder, "DawBackupError");
  gtk_label_set_text(GTK_LABEL(daw_conf_label), LoadDawConf);

  daw_conf_inprog = gtk_builder_get_object(builder, "DawConfWorkInProgress");
  daw_conf_done = gtk_builder_get_object(builder, "DawConfDone");
  daw_conf_done_img = gtk_builder_get_object(builder, "DawConfDoneImg");
  daw_conf_error_1 = gtk_builder_get_object(builder, "DawConfError1");
  daw_conf_error_2 = gtk_builder_get_object(builder, "DawConfError2");
  daw_conf_error_img = gtk_builder_get_object(builder, "DawConfErrorImg");
  daw_conf_empty = gtk_builder_get_object(builder, "DawConfEmpty");

  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(LockDawTopGrid), daw_top_button_grid);

  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_hide), daw_conf_label);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_hide), daw_conf_done);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_hide), daw_conf_done_img);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(gtk_widget_show), daw_conf_spinner_box);

  g_signal_connect(button_backup, "clicked",
                   G_CALLBACK(LoadPaths), NULL);
  g_signal_connect_swapped(button_backup, "clicked",
                           G_CALLBACK(DawConfSpinnerStart), daw_conf_spinner);

  g_signal_connect_swapped(daw_conf_close, "clicked",
                           G_CALLBACK(UnlockDawTopGrid), daw_top_button_grid);
  g_signal_connect_swapped(daw_conf_close, "clicked",
                           G_CALLBACK(DawBackupButton), daw_conf_close);

  g_signal_connect_swapped(daw_conf_cancel, "clicked",
                           G_CALLBACK(KillDawBackupRun), NULL);
  g_signal_connect_swapped(daw_conf_cancel, "clicked",
                           G_CALLBACK(DawBackupCancel), daw_conf_cancel);
  g_signal_connect(daw_conf_cancel, "clicked",
                   G_CALLBACK(DawBackupTrue), NULL);

  g_signal_connect(daw_backup_resume, "clicked",
                   G_CALLBACK(DawBackupFalse), NULL);
  g_signal_connect_swapped(daw_backup_resume, "clicked",
                           G_CALLBACK(DawResumeBackup), daw_backup_resume);
  g_signal_connect_swapped(daw_backup_resume, "clicked",
                           G_CALLBACK(gtk_widget_show), daw_conf_spinner_box);
  g_signal_connect_swapped(daw_backup_resume, "clicked",
                           G_CALLBACK(DawConfSpinnerStart), daw_conf_spinner);

  g_signal_connect(daw_backup_error, "clicked",
                   G_CALLBACK(DawBackupErrorTrue), NULL);
  g_signal_connect_swapped(daw_backup_error, "clicked",
                           G_CALLBACK(DawBackupErrorCloseButton), daw_backup_error);

  // DAW - Restore Button
  button_backup = gtk_builder_get_object(builder, "DawBackButton");
  button_restore = gtk_builder_get_object(builder, "DawRestoreButton");
  daw_back_close = gtk_builder_get_object(builder, "DawBackSpinnerBoxClose");
  daw_back_cancel = gtk_builder_get_object(builder, "DawBackSpinnerBoxCancel");

  daw_restore_resume = gtk_builder_get_object(builder, "DawRestoreResume");
  daw_restore_error = gtk_builder_get_object(builder, "DawRestoreErrorClose");

  daw_back_spinner_box = gtk_builder_get_object(builder, "DawBackSpinnerBox");
  daw_back_spinner = gtk_builder_get_object(builder, "DawBackSpinner");

  daw_back_label = gtk_builder_get_object(builder, "DawBackShowPath");
  daw_back_warning_label = gtk_builder_get_object(builder, "DawRestoreWarning");
  daw_back_error_label = gtk_builder_get_object(builder, "DawRestoreError");
  gtk_label_set_text(GTK_LABEL(daw_back_label), LoadDawBack);

  daw_back_inprog = gtk_builder_get_object (builder, "DawBackWorkInProgress");
  daw_back_done = gtk_builder_get_object (builder, "DawBackDone");
  daw_back_done_img = gtk_builder_get_object (builder, "DawBackDoneImg");
  daw_back_error_1 = gtk_builder_get_object(builder, "DawBackError1");
  daw_back_error_2 = gtk_builder_get_object(builder, "DawBackError2");
  daw_back_error_img = gtk_builder_get_object(builder, "DawBackErrorImg");
  daw_back_empty = gtk_builder_get_object(builder, "DawBackEmpty");

  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(LockDawTopGrid), daw_top_button_grid);

  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_hide), daw_back_label);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_hide), daw_back_done);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_hide), daw_back_done_img);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(gtk_widget_show), daw_back_spinner_box);

  g_signal_connect(button_restore, "clicked",
                   G_CALLBACK(LoadPaths), NULL);
  g_signal_connect_swapped(button_restore, "clicked",
                           G_CALLBACK(DawBackSpinnerStart), daw_back_spinner);

  g_signal_connect_swapped(daw_back_close, "clicked",
                           G_CALLBACK(UnlockDawTopGrid), daw_top_button_grid);
  g_signal_connect_swapped(daw_back_close, "clicked",
                           G_CALLBACK(DawRestoreButton), daw_back_close);

  g_signal_connect_swapped(daw_back_cancel, "clicked",
                           G_CALLBACK(KillDawRestoreRun), NULL);
  g_signal_connect_swapped(daw_back_cancel, "clicked",
                           G_CALLBACK(DawRestoreCancel), daw_back_cancel);
  g_signal_connect(daw_back_cancel, "clicked",
                   G_CALLBACK(DawRestoreTrue), NULL);

  g_signal_connect(daw_restore_resume, "clicked",
                   G_CALLBACK(DawRestoreFalse), NULL);
  g_signal_connect_swapped(daw_restore_resume, "clicked",
                           G_CALLBACK(DawResumeRestore), daw_restore_resume);
  g_signal_connect_swapped(daw_restore_resume, "clicked",
                           G_CALLBACK(gtk_widget_show), daw_back_spinner_box);
  g_signal_connect_swapped(daw_restore_resume, "clicked",
                           G_CALLBACK(DawBackSpinnerStart), daw_back_spinner);

  g_signal_connect(daw_restore_error, "clicked",
                   G_CALLBACK(DawRestoreErrorTrue), NULL);
  g_signal_connect_swapped(daw_restore_error, "clicked",
                           G_CALLBACK(DawRestoreErrorCloseButton), daw_backup_error);

  // Fife Choosers
  label = gtk_builder_get_object(builder, "RecDirShowPath");
  button = gtk_builder_get_object(builder, "RecDirChooser");
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(button), LoadRecDir);

  g_signal_connect(button, "file-set", G_CALLBACK(LoadPaths), NULL);
  g_signal_connect(button, "file-set", G_CALLBACK(NewRecDir), label);

  label = gtk_builder_get_object(builder, "RecBackShowPath");
  button = gtk_builder_get_object(builder, "RecBackChooser");
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(button), LoadRecBack);

  g_signal_connect(button, "file-set", G_CALLBACK(LoadPaths), NULL);
  g_signal_connect(button, "file-set", G_CALLBACK(NewRecBack), label);

  label = gtk_builder_get_object(builder, "DawConfShowPath");
  button = gtk_builder_get_object(builder, "DawDirChooser");
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(button), LoadDawConf);

  g_signal_connect(button, "file-set", G_CALLBACK(LoadPaths), NULL);
  g_signal_connect(button, "file-set", G_CALLBACK(NewDawConf), label);

  label = gtk_builder_get_object(builder, "DawBackShowPath");
  button = gtk_builder_get_object(builder, "DawBackChooser");
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(button), LoadDawBack);

  g_signal_connect(button, "file-set", G_CALLBACK(LoadPaths), NULL);
  g_signal_connect(button, "file-set", G_CALLBACK(NewDawBack), label);

  gtk_widget_show(GTK_WIDGET(window));

  g_object_unref(builder);

  g_free(LoadRecDir);
  g_free(LoadRecBack);
  g_free(LoadDawConf);
  g_free(LoadDawBack);
}
