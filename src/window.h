#ifndef __MAINWINDOW_H
#define __MAINWINDOW_H

#include <gtk/gtk.h>

#define CBH_MAIN_WINDOW_TYPE (cbh_main_window_get_type())
G_DECLARE_FINAL_TYPE (CBHMainWindow, cbh_main_window, CBH, MAIN_WINDOW, GtkWindow)

void OpenWindow (GtkApplication *app,
                 gpointer       data);

extern GObject *window;

extern GObject *rec_top_button_grid;
extern GObject *daw_top_button_grid;

extern GObject *rec_dir_warning_label;
extern GObject *rec_dir_error_label;
extern GObject *rec_dir_spinner;
extern GObject *rec_dir_inprog;
extern GObject *rec_dir_done;
extern GObject *rec_dir_done_img;
extern GObject *rec_dir_error_1;
extern GObject *rec_dir_error_2;
extern GObject *rec_dir_error_img;
extern GObject *rec_dir_empty;
extern GObject *rec_dir_close;
extern GObject *rec_dir_cancel;

extern GObject *rec_back_warning_label;
extern GObject *rec_back_error_label;
extern GObject *rec_back_spinner;
extern GObject *rec_back_inprog;
extern GObject *rec_back_done;
extern GObject *rec_back_done_img;
extern GObject *rec_back_error_1;
extern GObject *rec_back_error_2;
extern GObject *rec_back_error_img;
extern GObject *rec_back_empty;
extern GObject *rec_back_close;
extern GObject *rec_back_cancel;

extern GObject *daw_conf_warning_label;
extern GObject *daw_conf_error_label;
extern GObject *daw_conf_spinner;
extern GObject *daw_conf_inprog;
extern GObject *daw_conf_done;
extern GObject *daw_conf_done_img;
extern GObject *daw_conf_error_1;
extern GObject *daw_conf_error_2;
extern GObject *daw_conf_error_img;
extern GObject *daw_conf_empty;
extern GObject *daw_conf_close;
extern GObject *daw_conf_cancel;

extern GObject *daw_back_warning_label;
extern GObject *daw_back_error_label;
extern GObject *daw_back_spinner;
extern GObject *daw_back_inprog;
extern GObject *daw_back_done;
extern GObject *daw_back_done_img;
extern GObject *daw_back_error_1;
extern GObject *daw_back_error_2;
extern GObject *daw_back_error_img;
extern GObject *daw_back_empty;
extern GObject *daw_back_close;
extern GObject *daw_back_cancel;
#endif
